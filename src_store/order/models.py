import logging

import requests
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _


logger = logging.getLogger(__name__)


class Order(models.Model):
    """
    Model for Order instance. Used to store orders history in store_db
    Fields:
        - number (str): Order number generated manually
        - status (int): Order status selected from the list of available statuses STATUS_CHOICES
        - warehouse (int): Order warehouse selected from the list of available statuses WAREHOUSE_CHOICES
    """
    WAREHOUSE_CHOICES = [
        (1, _("Warehouse 1")),
        (2, _("Warehouse 2")),
        (3, _("Warehouse 3"))
    ]
    STATUS_CHOICES = [
        (1, _("In process")),
        (2, _("Stored")),
        (3, _("Send"))
    ]

    number = models.CharField(_('Order number'), max_length=20, unique=True)
    status = models.IntegerField(_("Status of order"), choices=STATUS_CHOICES, default=1)
    warehouse = models.IntegerField(_("Warehouse of order"), choices=WAREHOUSE_CHOICES, default=1)

    class Meta:
        verbose_name = _('Store order')
        verbose_name_plural = _('Store orders')

    def __str__(self):
        return f'{self.number}: {self.get_warehouse_display()} - {self.get_status_display()}'


@receiver(post_save, sender=Order, dispatch_uid="unique_identifier")
def order_post_save_receiver(sender, instance, created, *args, **kwargs):
    if created:
        try:
            URL_WAREHOUSE_API = 'http://host.docker.internal:8001/api/orders/'
            headers = {'Content-type': 'application/json'}
            data = {
                'number': instance.number,
                'status': instance.status,
                'store': 1
            }
            requests.post(url=URL_WAREHOUSE_API, json=data, headers=headers, timeout=5)
        except requests.exceptions.RequestException as e:
            logger.error(f'Exception {e}')
