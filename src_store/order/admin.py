from django.contrib import admin
from order.models import Order


@admin.register(Order)
class StoreAdmin(admin.ModelAdmin):
    list_display = ('number', 'status', 'warehouse')
    search_fields = ('number', )

    def get_readonly_fields(self, request, obj=None):
        """Custom readonly fields option to create a prohibition on editing after creation"""
        if obj:
            return ('number', 'status', 'warehouse')
        return ()
