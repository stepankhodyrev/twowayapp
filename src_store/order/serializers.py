from rest_framework import serializers

from .models import Order


class OrderSerializer(serializers.ModelSerializer):
    """
    Serializer for Order model
    Fields:
        - id (int): Id of Order model. Read only
        - number (str): Order number. Max 20 symbols
        - status (int): Id of choice from STATUS_CHOICES list
        - warehouse (int): Id of choice from WAREHOUSE_CHOICES list
    """
    class Meta:
        model = Order
        fields = ('id', 'number', 'status', 'warehouse')
        read_only_fields = ('id', )
