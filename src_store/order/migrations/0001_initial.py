# Generated by Django 4.2.3 on 2023-07-20 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(1, 'In process'), (2, 'Stored'), (3, 'Send')], default=1, verbose_name='Status of order')),
                ('warehouse', models.IntegerField(choices=[(1, 'Warehouse 1'), (2, 'Warehouse 2'), (3, 'Warehouse 3')], default=1, verbose_name='Warehouse of order')),
            ],
            options={
                'verbose_name': 'Store order',
                'verbose_name_plural': 'Store orders',
            },
        ),
    ]
