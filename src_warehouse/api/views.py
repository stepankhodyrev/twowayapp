from rest_framework import mixins
from rest_framework import generics

from order.models import Order
from order.serializers import OrderSerializer


class OrderListAPIView(generics.ListCreateAPIView, mixins.ListModelMixin, mixins.CreateModelMixin):
    """
    API View for retrieving Order model list
    Pagination: By default - 10 instances per page. Set in _project.settings REST_FRAMEWORK
    Methods:
        - get: Used to process GET requests and retrieving Order list
        - post: Used to process POST requests and creating new Order instance
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class OrderDetailAPIView(generics.RetrieveUpdateDestroyAPIView,
                         mixins.RetrieveModelMixin,
                         mixins.UpdateModelMixin,
                         mixins.DestroyModelMixin):
    """
    API View for retrieving Order model detail
    Methods:
        - get: Used to process GET requests and retrieving Order instance
        - put: Used to process PATCH and PUT requests and updating Order instance
        - delete: Used to process DELETE requests and deleting Order instance
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
