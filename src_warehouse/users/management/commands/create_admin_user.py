from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    help = 'Create superuser when starting'

    def handle(self, *args, **kwargs):
        if not User.objects.filter(is_superuser=True).exists():
            email = 'admin@admin.com'  # Укажите желаемый email для superuser
            password = 'QWE123qwe'   # Укажите желаемый пароль для superuser
            username = 'admin'  # Укажите желаемый пароль для superuser
            User.objects.create_superuser(email=email, password=password, username=username)
            self.stdout.write(self.style.SUCCESS('Superuser created successfully'))
        else:
            self.stdout.write(self.style.SUCCESS('Superuser already exist'))
