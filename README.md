# Two way django app

## Contents
- [Description of the project](#description-of-project)
- [Easy start](#for-run-the-project)
- [Endpoints](#endpoints)
- [Stack](#used-stack)
- [Commands](#all-commands)

## Description of project

Microservice platform for store and warehouse operation.

The application consists of two Django projects (Store, Warehouse). Running on ports 8000 and 8001, both projects have an Order model and are synchronized with each other when creating, editing or deleting instances of this class

## For run the project

- Copy env_template folder and paste as env
- Install docker and docker-compose if you dont have it
- Run docker desktop on your machine
- Go to project root folder
- Use ```make build``` command in terminal
- Use ```make up``` command in terminal
 
For stop server or find another functions after start, look up on [commands section](#all-commands)

## Endpoints
### Store project
- ```swagger/``` – Swagger docs path
- ```api/``` – Main path for all API endpoints
- ```api/orders/``` – Path for get list of all orders created by the owner or creating new instances
- ```api/orders/<str:order_number>``` – Path for get detail info about certain order by order number,
update fields on it or delete the instance

### Warehouse project
- ```swagger/``` – Swagger docs path
- ```api/``` – Main path for all API endpoints
- ```api/orders/``` – Path for get list of all orders created by the owner or creating new instances
- ```api/orders/<int:order_id>``` – Path for get detail info about certain order by order id,
update fields on it or delete the instance


## Used stack
This is a **Django Rest Framework** project. **PostgreSQL** is used as database.

**Docker-compose** on startup creating 4 services:
- store-db (PostgreSQL db on 5432 port)
- warehouse-db (PostgreSQL db on 5433 port)
- store-app (Second app on 8000 port)
- warehouse-app (First app on 8001 port)

You can see all dependencies on ```pyproject.toml``` file 

## All commands
- ```make build``` – command for building new docker container
- ```make up``` – command for run existing docker container
- ```make stop``` – command for terminate running container
- ```make restart``` – command for stop and up running container straightaway
- ```make destroy``` – command for delete existing docker container
- ```make log``` – command for displaying log from docker (all images) to terminal
- ```make makemigrations_store``` – command for making migrations on development for store project
- ```make makemigrations_warehouse``` – command for making migrations on development for warehouse project
- ```make migrate_store``` – command for access migrations on development for store project
- ```make migrate_warehouse``` – command for access migrations on development for warehouse project
- ```make test_store``` – command for run tests on store project
- ```make test_warehouse``` – command for run tests on warehouse project
