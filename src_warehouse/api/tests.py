from rest_framework.test import APITestCase
from rest_framework import status
from order.models import Order
from order.serializers import OrderSerializer


class OrderListAPIViewTestCase(APITestCase):
    """
    Class for test /api/orders/ path
    Test cases:
        – test_get_order_list: Case for testing get right len of response data and status code
        - test_create_order: Case for testing creating new order
    """
    def setUp(self):
        self.order_data = [
            {'number': '1001', 'status': 1, 'store': 1},
            {'number': '1002', 'status': 2, 'store': 2},
            {'number': '1003', 'status': 3, 'store': 3},
        ]
        for data in self.order_data:
            Order.objects.create(**data)

        self.url = '/api/orders/'

    def test_get_order_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), len(self.order_data))

    def test_create_order(self):
        new_order_data = {'number': 'Order1', 'status': 1, 'store': 1}
        response = self.client.post(self.url, new_order_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Order.objects.count(), len(self.order_data) + 1)
        self.assertEqual(Order.objects.get(number='Order1').status, 1)


class OrderDetailAPIViewTestCase(APITestCase):
    """
    Class for test /api/orders/{Order.id} path
    Test cases:
        – test_get_order_detail: Case for testing get right data and status code
        - test_update_order: Case for testing updating existing order
        - test_delete_order: Case for testing deleting existing order
    """
    def setUp(self):
        self.order = Order.objects.create(number='Order1', status=1, store=1)
        self.url = f'/api/orders/{self.order.id}'

    def test_get_order_detail(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = OrderSerializer(self.order)
        self.assertEqual(response.data, serializer.data)

    def test_update_order(self):
        updated_data = {'number': 'Order1', 'status': 2, 'store': 3}
        response = self.client.put(self.url, updated_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.order.refresh_from_db()
        self.assertEqual(self.order.status, 2)

    def test_delete_order(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Order.objects.filter(number='Order1').exists())
