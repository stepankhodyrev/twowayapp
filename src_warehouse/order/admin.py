from django.contrib import admin
from order.models import Order


@admin.register(Order)
class StoreAdmin(admin.ModelAdmin):
    list_display = ('number', 'status', 'store')
    readonly_fields = ('number', 'store')
